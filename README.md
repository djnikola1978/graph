### Installing

Go to your web directory and type:
```
git clone https://djnikola1978@bitbucket.org/djnikola1978/seats-distribution.git
```

then go into seats-distribution/ folder:
```
cd seats-distribution/
```

then create a database:
```
php bin/console doctrine:database:create
```

and update database schema:
```
php bin/console doctrine:schema:update --force
```

## Running the tests

## Documentation 


## Built With

* [Symfony](https://symfony.com/) - The web framework used Symfony 4

## Authors

* **Nikola Dordevic**
