<?php

namespace App\Controller;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\Node;
use App\Entity\Graph;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Node controller.
 *
 * @Route("/api")
 */
class NodeController extends FOSRestController
{
    
    /**
     * @FOSRest\Post("/graph/node/")
     * 
     * @param Request $request
     */
    public function postNodeAction(Request $request)
    {
        
        $data = json_decode($request->getContent(), true);
        $node = new \App\Entity\Node();
        $node->setName($data['name']);
        
        $graph = $this->container->get('graph_handler');
        $graph->save($node);
        return View::create($node, Response::HTTP_OK);
        
    }
    
    /**
     * @FOSRest\Get("/graph/node/{node_id}")
     * 
     * @param int $node_id
     */
    public function getNodeAction(int $node_id)
    {
        
        $graph = $this->container->get('graph_handler');
        $node = $graph->get($node_id);
        if (null === $node)
            return View::create("Node: " . $node_id . " not found!", Response::HTTP_NOT_FOUND);
        return View::create($node, Response::HTTP_OK);
    }
    
    /**
     * @FOSRest\Delete("/graph/node/{node_id}")
     * 
     * @param int $node_id
     */
    public function deleteNodeAction(int $node_id)
    {
        
        $graph = $this->container->get('graph_handler');
        $node = $graph->get($node_id);
        if (null === $node)
            return View::create("Node: " . $node_id . " not found!", Response::HTTP_NOT_FOUND);
        $graph->delete($node);
        return View::create($node, Response::HTTP_OK);
    }
    
    /**
     * @FOSRest\Put("/graph/node/{node_id}")
     * 
     * @param int $node_id
     */
    public function updateNodeAction(Request $request, int $node_id)
    {
        
        $graph = $this->container->get('graph_handler');
        $node = $graph->get($node_id);
        if (null === $node)
            return View::create("Node: " . $node_id . " not found!", Response::HTTP_NOT_FOUND);
        
        $data = json_decode($request->getContent(), true);
        $node->setName($data['name']);
        $graph->save($node);
        return View::create($node, Response::HTTP_OK);
    }
    
    /**
     * @FOSRest\Post("/graph/connect/{source_id}/{destination_id}")
     * 
     * @param int $source_id
     * @param int $destination_id
     */
    public function connectNodeAction(int $source_id, int $destination_id)
    {
        
        $graph = $this->container->get('graph_handler');
        $sourceNode = $graph->get($source_id);
        if (null === $sourceNode)
            return View::create("Node: " . $source_id . " not found!", Response::HTTP_NOT_FOUND);
        
        $destinationNode = $graph->get($destination_id);
        if (null === $destinationNode)
            return View::create("Node: " . $destination_id . " not found!", Response::HTTP_NOT_FOUND);
        
        $sourceNode->addChildNode($destinationNode);
        $graph->save($sourceNode);
        return View::create($sourceNode, Response::HTTP_OK);
    }
}