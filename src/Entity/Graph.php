<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use App\Entity\Interfaces\GraphInterface;
use App\Entity\Interfaces\NodeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Graph
 *
 * @author Nikola
 */
class Graph implements GraphInterface {
    
    /**
     *
     * @var array
     */
    private $_nodes;
    
    function __construct() {
        $this->_nodes = [];
    }
    
    /**
     * Add node to graph. 
     * 
     * @param NodeInterface $node
     * @throws \Exception
     */
    function addNode(NodeInterface $node) {
        if (key_exists($node->getId(), $this->_nodes)) 
            throw new \Exception("Node: " . $node->getId(). " already exists!");
        
        $this->_nodes[$node->getId()] = $node;
    }
    
    /**
     * Get node by id.
     * 
     * @param int $id
     * @return boolean
     */
    function getNodeById(int $id) {
        if (!key_exists($id, $this->_nodes)) 
            return false;
        
        return $this->_nodes[$id];
    }
    
    /**
     * Returns true if graph is empty.
     * 
     * @return bool
     */
    function isEmpty() : bool {
        return (count($this->_nodes) === 0);
    }
    
    /**
     * Returns number of nodes in graph.
     * 
     * @return int
     */
    function getNodesCount() : int {
        return count($this->_nodes);
    }
    
    /**
     * Connects two node. 
     * 
     * @param int $sourceNodeId
     * @param int $destinationNodeId
     * @throws \Exception
     */
    function connectNodes(int $sourceNodeId, int $destinationNodeId)
    {
        $sourceNode = $this->getNodeById($sourceNodeId);
        if (false === $sourceNode) {
            throw new \Exception("Sorce node: " . $sourceNodeId . " does not exists!");
        }
        
        $destinationNode = $this->getNodeById($destinationNodeId);
        if (false === $destinationNode) {
            throw new \Exception("Destination node: " . $destinationNodeId . " does not exists!");
        }
        
        $sourceNode->addChildNode($destinationNode);
    }
    
    /**
     * 
     * @param NodeInterface $sourceNode
     * @param NodeInterface $destinationNode
     * @param array $current_path
     * @param array $all_paths
     */
    private function _findPaths(NodeInterface $sourceNode, NodeInterface $destinationNode,  
            array & $current_path, array & $all_paths) {
        
        $childNodes = $sourceNode->getChildNodes()->toArray();
        
        if (is_array($childNodes) && 0 !== count($childNodes))
        {
            foreach($childNodes as $childNode) {
                $current_path[] = $childNode->getId();            

                if ($destinationNode->getId() === $childNode->getId()) {
                    $all_paths[] = $current_path;
                    array_pop($current_path);
                }
                else {
                    $this->_findPaths($childNode, $destinationNode, $current_path, $all_paths);
                }

            }
        }
        array_pop($current_path);
    }
    
    /**
     * 
     * @param int $sourceNodeId
     * @param int $destinationNodeId
     * @return boolean|array
     */
    function findShortestPath(int $sourceNodeId, int $destinationNodeId) {
        if ($this->isEmpty())
            return false;

        
        $sourceNode = $this->getNodeById($sourceNodeId);
        if (false === $sourceNode)
            return false;
        
        $destinationNode = $this->getNodeById($destinationNodeId);
        if (false === $destinationNode)
            return false;
        
        if (true === $sourceNode->isLeaf())
            return false;
        
        $all_paths = [];
        $current_path = [];
        $current_path[] = $sourceNode->getId();
        
        $this->_findPaths($sourceNode, $destinationNode, $current_path, $all_paths);
        if (!is_array($all_paths) || 0 === count($all_paths))
            return false;
       
        if (1 === count($all_paths))
            return $all_paths[0];
        
        $min_path = null;
        foreach($all_paths as $path) {
            if (count($path) < $min_path || $min_path === null)
                $min_path = $path;
        }
        return $min_path;
    }
}
