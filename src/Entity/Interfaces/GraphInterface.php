<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Entity\Interfaces;
/**
 *
 * @author Nikola
 */
interface GraphInterface {
    
    /**
     * Add node to graph. 
     * 
     * @param NodeInterface $node
     * @throws \Exception
     */
    function addNode(NodeInterface $node);
    
    /**
     * Get node by id.
     * 
     * @param int $id
     * @return boolean
     */
    function getNodeById(int $id);
    
    /**
     * Returns true if graph is empty.
     * 
     * @return bool
     */
    function isEmpty() : bool;
    /**
     * Returns number of nodes in graph.
     * 
     * @return int
     */
    function getNodesCount() : int;
    
    /**
     * Connects two node. 
     * 
     * @param int $sourceNodeId
     * @param int $destinationNodeId
     * @throws \Exception
     */
    function connectNodes(int $sourceNodeId, int $destinationNodeId);
    
    
    /**
     * Find shortest oath from sourceNodeId to destinationNodeId
     * 
     * @param int $sourceNodeId
     * @param int $destinationNodeId
     */
    function findShortestPath(int $sourceNodeId, int $destinationNodeId);
}
