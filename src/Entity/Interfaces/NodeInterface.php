<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Entity\Interfaces;
/**
 *
 * @author Nikola
 */
interface NodeInterface {
    
    function getId() : int;

    function getName() : string;

    function getAttributes() : string;
    
    function getChildNodes();

    function setId(int $id);

    function setName(string $name);

    function setAttributes(string $attributes);
    
    function setChildNodes($childNodes);
    
    public function addChildNode(NodeInterface $node);

    public function isLeaf();
    
    public function isConnected(NodeInterface $node);
}
