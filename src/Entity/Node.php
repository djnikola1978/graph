<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Entity;

use App\Entity\Interfaces\NodeInterface;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="node")
 * 
 */
class Node implements NodeInterface {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var int
     */
    private $_id;
    
    /**
     * @ORM\Column(name="name", type="string", length=100)
     *
     * @var string
     */
    private $_name;
    
    /**
     * @ORM\Column(name="attributes", type="string")
     * 
     * @var text
     */
    private $_attributes;
    
    /**
     *
     * @ORM\ManyToMany(targetEntity="Node")
     * @ORM\JoinTable(name="node_connection",
     *      joinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")}
     *      )
     */
    private $_childNodes;
    
    
    function __construct() {
        $this->_childNodes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->_attributes = '';
    }
    
    function getId() : int {
        return $this->_id;
    }

    function getName() : string {
        return $this->_name;
    }

    function getAttributes() : string {
        return $this->_attributes;
    }
    
    function getChildNodes() {
        return $this->_childNodes;
    }
    
    function setId(int $id) {
        $this->_id = $id;
        return $this;
    }

    function setName(string $name) {
        $this->_name = $name;
        return $this;
    }

    function setAttributes(string $attributes) {
        $this->_attributes = $attributes;
        return $this;
    }
    
    function setChildNodes($childNodes) {
        $this->_childNodes = $childNodes;
        return $this;
    }
    
    public function addChildNode(NodeInterface $node) {
        
        if ($this->_childNodes->contains($node)) {
            throw new \Exception("Node: " . $node->getId() . " is already connected with node: " . $node->getId() . " !");
        }
        $this->_childNodes->add($node);
        return $this;
    }
    
    /**
     * Returns true if node is a leaf.
     * 
     * @return bool
     */
    public function isLeaf() {
        return (0 === count($this->_childNodes));
    }
    
    /**
     * Returns true if node is connected with $node.
     * 
     * @param NodeInterface $node
     */
    public function isConnected(NodeInterface $node) {
        return ($this->_childNodes->contains($node));
    }
}
