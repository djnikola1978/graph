<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Repository;

use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Interfaces\NodeInterface;

/**
 * Description of GraphHandler
 *
 * @author Nikola
 */
class GraphHandler {
    /**
     *
     * @var \Doctrine\Common\Persistence\ObjectManager 
     */
    private $om;
    
    /**
     *
     * @var string
     */
    private $entityClass;
    
    /**
     *
     * @var \Doctrine\Common\Persistence\ObjectRepository 
     */
    private $repository;
    
    /**
     * 
     * @param ObjectManager $om
     * @param string $entityClass
     */
    public function __construct(\Doctrine\Common\Persistence\ObjectManager $om, $entityClass)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
    }
    
    /**
     * 
     * {@inheritdoc}
     */
    public function getAll() 
    {
        $allStates = $this->om->getRepository($this->entityClass)->findAll();
        return $allStates;
    }

    /**
     * 
     * {@inheritdoc}
     */
    public function get(int $nodeId) 
    {
        $node = $this->om->getRepository($this->entityClass)->find($nodeId);
        return $node;
    }

    /**
     * 
     * {@inheritdoc}
     */
    public function save(NodeInterface $node) 
    {
        $this->om->persist($node);
        $this->om->flush();

    }
    
    /**
     * 
     * {@inheritdoc}
     */
    public function delete(NodeInterface $node) 
    {
        $this->om->remove($node);
        $this->om->flush();
    }
    
    public function __toString(){
        return "";
    }
    
}
