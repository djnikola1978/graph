<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NodeControllerTest extends WebTestCase
{
    /**
     * Tests GET Method.
     */
    public function testGetIndex()
    {
        
        $client = static::createClient();

        $client->request('GET', '');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
    }
}