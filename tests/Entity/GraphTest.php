<?php
namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Node;
use App\Entity\Graph;

class GraphTest extends TestCase
{
    /**
     * Adding and reading node.
     */
    public function testReadNodeTrue()
    {
        $graph = new Graph();
        
        $a = new Node();
        $a->setId(1)->setName("A");
        $graph->addNode($a);
        
        $_a = $graph->getNodeById($a->getId());
        $this->assertNotFalse($_a);        
    }
    
    /**
     * Testing adding duplicate node.
     * 
     * @expectedException \Exception
     */
    public function testAddDuplicateNodeFalse()
    {
        $graph = new Graph();
        
        $a = new Node();
        $a->setId(1)->setName("A");
        $graph->addNode($a);
        
        $b = new Node();
        $b->setId(1)->setName("B");
        $graph->addNode($b);
        
    }
    
    /**
     * Testing 
     * 
     */
    public function testConnectingNodesTrue()
    {
        $graph = new Graph();
        
        $a = new Node();
        $a->setId(1)->setName("A");
        $graph->addNode($a);
        
        $b = new Node();
        $b->setId(2)->setName("B");
        $graph->addNode($b);
        $graph->connectNodes($a->getId(), $b->getId());       
        $this->assertTrue($a->isConnected($b), "Node: A should be connected to node: B !");
    }
    
    public function testPathsNodeTrue()
    {
        $graph = new Graph();
        
        $a = new Node();
        $a->setId(1)->setName("A");
        $graph->addNode($a);
        $b = new Node();
        $b->setId(2)->setName("B");
        $graph->addNode($b);
        $c = new Node();
        $c->setId(3)->setName("C");
        $graph->addNode($c);
        $d = new Node();
        $d->setId(4)->setName("D");
        $graph->addNode($d);
        $e = new Node();
        $e->setId(5)->setName("E");
        $graph->addNode($e);
        $f = new Node();
        $f->setId(6)->setName("F");
        $graph->addNode($f);
        
        $graph->connectNodes($a->getId(), $b->getId());
        $graph->connectNodes($b->getId(), $c->getId());
        $graph->connectNodes($c->getId(), $d->getId());
        $graph->connectNodes($d->getId(), $e->getId());
        $graph->connectNodes($a->getId(), $d->getId());
        $graph->connectNodes($a->getId(), $f->getId());
        
        $path_shortest = $graph->findShortestPath($a->getId(), $e->getId());
        $this->assertEquals(array(1, 4, 5), $path_shortest, "The shortest path should be: 1, 4, 5");
    }
     
}